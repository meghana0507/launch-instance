# 
# Configure AWS credentials for aws commands to work
#
export AWS_ACCESS_KEY_ID=$1  # pass your aws access ID as 1st parameter to this script
export AWS_SECRET_ACCESS_KEY=$2  # pass your aws secret key as 2nd parameter to this script
export AWS_DEFAULT_REGION=us-west-2
export AWS_DEFAULT_OUTPUT=text

export PATH=~/bin:$PATH 

#
# Install AWS-CLI to run aws commands
#
aws --version > /dev/null 2>&1
RC=$?
if [ $RC -ne 0 ]; then
    echo "Installing AWS-CLI..."  
    curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
    unzip awscli-bundle.zip
    ./awscli-bundle/install -b ~/bin/aws   

    export PATH=~/bin:$PATH
fi

#
# Make sure AWS connection is successful with the credentials provided
#
aws ec2 describe-regions --output table > /dev/null 2>&1
RC=$?
if [ $RC -ne 0 ]; then
    echo "ERROR: Unable to connect to AWS"
    exit 1
else
    echo "AWS connection was successfully established"    
fi


#
# AWS Instance configuration variables
#
SEC_GROUP_NAME="linux-sg"
KEYPAIR_NAME="linux-ssh-key"
IMAGE_ID="ami-036affea69a1101c9"


#
# Create Security group and rules if it doesn't exist on AWS already
#
aws ec2 describe-security-groups --group-names ${SEC_GROUP_NAME} > /dev/null 2>&1
RC=$?
if [ $RC -ne 0 ]; then
    SEC_GROUP_ID=`aws ec2 create-security-group --group-name ${SEC_GROUP_NAME} --description "Basic Security group rules"`

    #aws ec2 authorize-security-group-ingress --group-name ${SEC_GROUP_NAME} --protocol tcp --port 22 --cidr <MYIP>
    aws ec2 authorize-security-group-ingress --group-name ${SEC_GROUP_NAME} --protocol tcp --port 22 --cidr 0.0.0.0/0
    aws ec2 authorize-security-group-ingress --group-name ${SEC_GROUP_NAME} --protocol tcp --port 80 --cidr 0.0.0.0/0
    aws ec2 authorize-security-group-ingress --group-name ${SEC_GROUP_NAME} --protocol tcp --port 443 --cidr 0.0.0.0/0

    echo "Security Group created: ${SEC_GROUP_ID}" 
else
    SEC_GROUP_ID=`aws ec2 describe-security-groups --group-names ${SEC_GROUP_NAME} --query "SecurityGroups[*].GroupId"`
    echo "Security Group ${SEC_GROUP_NAME} already exists!"
fi


#
# Generate Keypair if it doesn't exist on AWS already
#
aws ec2 describe-key-pairs --key-name ${KEYPAIR_NAME} > /dev/null 2>&1
RC=$?
if [ $RC -ne 0 ]; then
    echo "Generating key ${KEYPAIR_NAME}.pem"
    aws ec2 create-key-pair --key-name ${KEYPAIR_NAME} --query "KeyMaterial" --output text > ${KEYPAIR_NAME}.pem
    chmod 400 ${KEYPAIR_NAME}.pem

    echo "Generating public ssh key..."
    ssh-keygen -y -f ./${KEYPAIR_NAME}.pem > authorized_keys # also getting public key for users' ssh access
else
    echo "Keypair ${KEYPAIR_NAME}.pem already exists!"
fi


#
# Launching new instance on AWS with the configuration provided
#
export INSTANCE_ID=`aws ec2 run-instances --image-id ${IMAGE_ID} --security-group-ids ${SEC_GROUP_ID} --count 1 --instance-type t2.micro --key-name ${KEYPAIR_NAME} --query "Instances[0].InstanceId"`
RC=$?
if [ $RC -ne 0 ]; then
    echo "ERROR: Launching instance failed with code [${RC}]"
    exit 1
fi
echo "Instance id: ${INSTANCE_ID}"


#
# Wait and Try for 5 times until IP address of instance is acquired
#
IP_ADDRESS=""
LOOP_COUNT=1

while [[ "${IP_ADDRESS}" == "" && ${LOOP_COUNT} -lt 6 ]]
do
    echo "Getting IP Address of New Instance ${LOOP_COUNT}/5..."
    IP_ADDRESS=`aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --query "Reservations[0].Instances[0].PublicIpAddress"`
    sleep 5
    LOOP_COUNT=`expr ${LOOP_COUNT} + 1`
done

if [ -z "${IP_ADDRESS}" ]; then
    echo "ERROR: Unable to acquire IP Address"
    exit 1
else
    echo "Acquired IP Address: ${IP_ADDRESS}"
fi


#
# Wait and Try for 10 times until SSH connection is established
#
UP_TIME=""
SSH_COUNT=1
while [[ "${UP_TIME}" == "" && ${SSH_COUNT} -lt 11 ]]
do
    echo "Trying to establish SSH connection to New Instance ${SSH_COUNT}/1O..."
    UP_TIME=`ssh -i ./${KEYPAIR_NAME}.pem -o "StrictHostKeyChecking no" ec2-user@${IP_ADDRESS} "sudo uptime"`
    sleep 15
    SSH_COUNT=`expr ${SSH_COUNT} + 1`
done


#
# SSH to new instance to install basic tools and add users  
#
if [ -z "${UP_TIME}" ]; then
    echo "ERROR: SSH connection to host got refused"
    exit 1
else
    echo "SSH connection successfully established"

    # Install basic tools like Chef and git
    ssh -i ./${KEYPAIR_NAME}.pem -o "StrictHostKeyChecking no" ec2-user@${IP_ADDRESS} "sudo yum -y install https://packages.chef.io/files/stable/chefdk/3.6.57/el/7/chefdk-3.6.57-1.el7.x86_64.rpm; sudo yum -y install git"

    # Add Users to SSH access using pem key
    USER="megh"
    scp -i ./${KEYPAIR_NAME}.pem authorized_keys ec2-user@${IP_ADDRESS}:/tmp/
    ssh -i ./${KEYPAIR_NAME}.pem ec2-user@${IP_ADDRESS} "sudo adduser ${USER}; sudo mkdir /home/${USER}/.ssh; sudo chmod 700 /home/${USER}/.ssh; sudo cp /tmp/authorized_keys /home/${USER}/.ssh/; sudo chmod 600 /home/${USER}/.ssh/authorized_keys; sudo chown -R ${USER}:${USER} /home/${USER}"
fi


#
# Unset variables
#
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_DEFAULT_REGION
unset AWS_DEFAULT_OUTPUT

unset INSTANCE_ID